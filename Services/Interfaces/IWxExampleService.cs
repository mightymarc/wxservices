using System;
using System.Collections.Generic;
using OpenSim.Server.Base;
using OpenSim.Services.Interfaces;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using Wx.Data;


namespace Wx.Services.Interfaces
{
    public interface IWxExampleService
    {
        IWxMyData WxDb
        {
            get;
        }

        OSDMap PutWxUser(Dictionary<string, object> request);
        OSDMap ListWxUser();
        UserAccount GetUserData(UUID userID);
    }
}
