using System;
using System.Collections.Generic;
using OpenMetaverse.StructuredData;
using System.Text;



namespace Wx.Server.Handlers
{
    public class Util
    {
        public byte[] FailureResult()
        {
            return FailureResult(String.Empty);
        }

        public byte[] FailureResult(string msg)
        {
            // m_log.ErrorFormat("[WxUserHandler] " + msg);
            OSDMap doc = new OSDMap(2);
            doc["Result"] = OSD.FromString("Failure");
            doc["Message"] = OSD.FromString(msg);

            return DocToBytes(doc);
        }

        public byte[] SuccessResult(OSDMap response)
        {
            response["Result"] = OSD.FromString("Success");
            return DocToBytes(response);
        }

        public byte[] DocToBytes(OSDMap doc)
        {
            return Encoding.UTF8.GetBytes(OSDParser.SerializeJsonString(doc));
        }
    }
}
