/*
** Copyright 2011 BlueWall Information Technologies, LLC
**
**   Licensed under the Apache License, Version 2.0 (the "License");
**   you may not use this file except in compliance with the License.
**   You may obtain a copy of the License at
**
**       http://www.apache.org/licenses/LICENSE-2.0
**
**   Unless required by applicable law or agreed to in writing, software
**   distributed under the License is distributed on an "AS IS" BASIS,
**   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
**   See the License for the specific language governing permissions and
**   limitations under the License.
*/

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Nini.Config;
using log4net;
using OpenSim.Framework.Servers.HttpServer;
using OpenSim.Server.Base;
using OpenSim.Services.Interfaces;
using OpenMetaverse;
using OpenMetaverse.StructuredData;
using Wx.Services.Interfaces;
using Wx.Server;
using Wx.Data;
using Wx.Server.Handlers;

namespace Wx.Server.Handlers
{
    public class WxExampleHandlers: BaseStreamHandler
    {
        IWxExampleService m_Service;
        private static readonly ILog m_log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        Util util = new Util();

        public WxExampleHandlers(IWxExampleService service) : base("POST", "/WxExample")
        {
            m_Service = service;
            m_log.Info("[WxExampleHandler]: Loading");
        }

        public override byte[] Handle(string path, Stream requestData, OSHttpRequest httpRequest, OSHttpResponse httpResponse)
        {
            StreamReader sr = new StreamReader(requestData);
            string body = sr.ReadToEnd();
            sr.Close();
            body = body.Trim();

            try
            {
                Dictionary<string, object> request = ServerUtils.ParseQueryString(body);

                m_log.DebugFormat("[WxExampleHandler]: Handler {0}", body.ToString());

                if (!request.ContainsKey("METHOD"))
                    return util.FailureResult("Error, no method defined!");
                string method = request["METHOD"].ToString();

                // Look for our caller's method...
                switch (method)
                {
                    case "testing":
                        return TestResponse(request);

                    case "put_wxuser":
                        return PutWxUser(request);

                    case "list_wxuser":
                        return ListWxUser();

                    case "get_user_info":
                        return GetUserInfo(request);

                    default:
                        m_log.DebugFormat("[WxUserHandler]: unknown method {0} request {1}", method.Length, method);
                        return util.FailureResult("WxUsersHandler: Unrecognized method requested!");
                }
            }
            catch (Exception e)
            {
                m_log.DebugFormat("[Wx HANDLER]: Exception {0}", e);
            }

            return util.FailureResult();
        }

//        #region utility
//        private byte[] FailureResult()
//        {
//            return FailureResult(String.Empty);
//        }
//
//        private byte[] FailureResult(string msg)
//        {
//            OSDMap doc = new OSDMap(2);
//            doc["Result"] = OSD.FromString("Failure");
//            doc["Message"] = OSD.FromString(msg);
//
//            return DocToBytes(doc);
//        }
//
//        private byte[] DocToBytes(OSDMap doc)
//        {
//            return Encoding.UTF8.GetBytes(OSDParser.SerializeJsonString(doc));
//        }
//        #endregion utility

        #region Handler Methods
        private byte[] PutWxUser(Dictionary<string, object> request)
        {
            return util.DocToBytes(m_Service.PutWxUser(request));
        }

        private byte[] ListWxUser()
        {
            OSDMap ret_val = m_Service.ListWxUser();

            if ( ret_val["result"] == "success" )
                return util.DocToBytes(ret_val);

            return util.FailureResult();
        }

        private byte[] TestResponse (Dictionary<string, object> request)
        {
            OSDMap doc = new OSDMap(request.Count + 1);
            if ( request.ContainsKey("HELLO"))
            {
                m_log.InfoFormat("[Wx]: Users Testing {0}", request["HELLO"].ToString());
                doc["Greeting"] = OSD.FromString("Goodbye!");

                foreach (KeyValuePair<string, object> item in request) {

                    doc[item.Key] = OSD.FromString(item.Value.ToString());

                }

                return util.DocToBytes(doc);
            }
            return util.FailureResult("You must say HELLO!");
        }

        private byte[] GetUserInfo(Dictionary<string, object> request)
        {
            UUID id = UUID.Zero;
            UserAccount d = null;

            if ( request.ContainsKey("user_id"))
            {
                if (UUID.TryParse(request["user_id"].ToString(), out id))
                {
                    d = m_Service.GetUserData(id);
                }
                else
                {
                    return util.FailureResult(String.Format("Error getting userID {0}", id));
                }
            }

            if ( d != null )
            {
                Dictionary<string, object> userData = d.ToKeyValuePairs();
                OSDMap doc = new OSDMap(userData.Count);

                foreach (KeyValuePair<string, object> item in userData) {

                    doc[item.Key] = OSD.FromString(item.Value.ToString());

                }
                return util.DocToBytes(doc);
            }
            return util.FailureResult("Error getting user info");
        }
        #endregion Handler Methods
    }
}
